//
//  ViewController.swift
//  Ejemplo notificacion programada
//
//  Created by Francesc Roig ...
//  Copyright © 2018 Francesc Roig i Feliu. All rights reserved.
//

import UIKit
import UserNotifications    // Necesario para las notificaciones.


// IMPORTANTE! Ver también el fichero "ViewController.swift"

/*
 * Bloque principal, también llamado clase.
 */
class ViewController: UIViewController {
    
    /*
     * Esta función se invoca automáticamente cuando aparece la pantalla asociada
     * a este fitchero (también llamado controlador).
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*
         Las notificaciones se muetran independientemente de si la aplicación está en primer plano
         o no se está ejecutando en el momento de ser lanzadas, por lo que es preciso solicitar al
         usuario permiso para poder recibir notificaciones de esta aplicación.
         */
        UNUserNotificationCenter.current().requestAuthorization( options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in  })
        
    }
    
    
    
    /*
     * Acción de pulsación del botón 'Programar notificación'.
     */
    @IBAction func botoNotificacio(_ sender: Any) {
        /*
         Se declara y se inicializa el componente correspondiente al centro notificaciones.
         Este componente es el encargado de insertar la notificaciones en el sistema operativo
         para que éste las mueste en pantalla.
         */
        let centro = UNUserNotificationCenter.current()
        
        /*
         Las notificaciones se muetran independientemente de si la aplicación está en primer plano
         o no se está ejecutando en el momento de ser lanzadas, por lo que es preciso solicitar al
         usuario permiso para poder recibir notificaciones de esta aplicación.
         */
        centro.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Se comprueba (si 'granted' es true) si se ha concedido permiso a la aplicación para enviar notificaciones.
            if granted {
                /*
                 Se ha comprobado que el usuario ha concedido el permiso para recibir
                 notificaciones de esta aplicación, con lo que ahora estas ya se puede definir
                 y añadir al centro de notificaciones para que el sistema operativo las
                 lance segun su programación (timeInterval).
                 */
            
                // Define la notificación
                let content = UNMutableNotificationContent()
                content.title = "Título notificación !!!"
                content.body = "Cuerpo de la notificación"
                content.sound = UNNotificationSound.default()
                
                /*
                 Crea una notificación que se repite, en este caso, cada 60 segundos
                 ya que 'repeats: true'. El periodo mínimo ajustable es de 60s si
                 la notificación és periódica.
                 
                 En el caso de 'repeats: false', la notificaión será lanzada el número de segundos
                 especificado después de que la petición (request) sea añadida al centro de notoficaciones.
                */
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
                
                // Crea la petición de notificación para poderla añadir al centro de notificaciones.
                let request = UNNotificationRequest(identifier: "Mi notificación", content: content, trigger: trigger)
                
                // Añade la notificación al centro de notificaciones del sistema desde  donde será lanzada.
                centro.add(request)
            } else {
                /*
                 El usuario na ha otorgado permiso a la aplicación para que les pueda
                 mandar notificaciones.
                 */
                print(" No se ha concedido permiso para enviar notificaciones !!!")
            }
        }
    }
    
    
    
} // Final bloque principal.
